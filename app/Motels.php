<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Motels extends Model
{
    use SoftDeletes;
    protected $table = "motels";
    protected $fillable =[
        'name',
        'address'
    ];
    protected $dates = ['deleted_at'];
}
